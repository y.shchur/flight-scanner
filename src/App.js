import React from 'react';
import './App.scss';
import {AppRouting} from "./AppRouting";
import {Header} from "shared";


export class App extends React.Component {

  render() {
    return (
      <div>
        <Header/>
        <AppRouting/>
      </div>
    );
  }
}
