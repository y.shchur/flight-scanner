import React from 'react';
import 'ol/ol.css';
import arc from 'arc'
import {Map, View} from 'ol';
import WKT from 'ol/format/WKT'
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import {AirportService} from '../service/AirportService';
import Feature from 'ol/Feature';
import LineString from 'ol/geom/LineString';
import Style from 'ol/style/Style';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import * as proj from 'ol/proj';
import Point from 'ol/geom/Point';
import Icon from 'ol/style/Icon';


export class FlightMap extends React.Component {

  _airportService;

  map;
  airportLayer;
  flightLayer;

  constructor(props, context) {
    super(props, context);
    this._airportService = AirportService.getInstance();
    this.state = {
      airports: []
    };
  }

  componentDidMount() {
    this._createMap();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this._addSelectedFlightsToMap(this.props.selectedFlights)
  }

  _addSelectedFlightsToMap(selectedFlights) {
    this.airportLayer.clear();
    this.flightLayer.clear();
    this._airportService.airports$.then(airports => {
      selectedFlights.forEach(flight => {
        const wkt = new WKT();
        const originGeom = wkt.readGeometry(airports.get(flight.originCode).geom);
        this.airportLayer.addFeature(this._createAirport(originGeom, 'TAKE_OFF'));
        const destinationGeom = wkt.readGeometry(airports.get(flight.destinationCode).geom);
        this.airportLayer.addFeature(this._createAirport(destinationGeom, 'TOUCH_DOWN'));
        this.flightLayer.addFeature(this._createLine(originGeom.getCoordinates(), destinationGeom.getCoordinates()));
      })
    })
  }

  _createAirport(geom, type) {
    const airport = new Feature(new Point(geom.getCoordinates()).transform('EPSG:4326', 'EPSG:3857'));
    const src = type === 'TAKE_OFF' ? '/take_off.svg' : '/touch_down.svg';
    airport.setStyle(new Style({
      image: new Icon({
        src: `${process.env.PUBLIC_URL}` + src
      })
    }));
    console.log(airport.getStyle().getImage());
    return airport;
  }

  _createLine(fromCoordinates, toCoordinates) {
    const arcLine = new arc.GreatCircle(
      {x: fromCoordinates[0], y: fromCoordinates[1]},
      {x: toCoordinates[0], y: toCoordinates[1]}).Arc(100, {offset: 10});
    const line = new Feature(new LineString(arcLine.geometries[0].coords).transform('EPSG:4326', 'EPSG:3857'));
    return line;
  }

  _createMap() {
    this.airportLayer = new VectorSource({
      features: this.state.airports
    });

    this.flightLayer = new VectorSource({
      features: []
    });

    this.map = new Map({
      target: this.refs.mapContainer,
      layers: [
        new TileLayer({
          source: new OSM()
        }),
        new VectorLayer({
          source: this.flightLayer,
          style: new Style({
            fill: new Fill({color: '#7a77ff', weight: 4}),
            stroke: new Stroke({color: '#7a77ff', width: 2})
          })
        }),
        new VectorLayer({
          source: this.airportLayer,
        })
      ],
      view: new View({
        center: proj.transform([16, 51], 'EPSG:4326', 'EPSG:3857'),
        zoom: 5
      })
    });
  }

  render() {
    return (
      <div ref="mapContainer"></div>
    );
  }

}
