import * as React from "react";
import {SearchCriteria} from "./SearchCriteria";
import {SearchResults} from "./SearchResults";
import {FlightService} from "../service/FlightService";
import {FlightMap} from './FlightMap';
import style from './style/FlightOverview.module.scss'

export class FlightOverview extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      flights: [],
      selectedFlights: [],
      airports: []
    };
    this.handleCriteriaSearch = this.handleCriteriaSearch.bind(this);
    this.handleRowClick = this.handleRowClick.bind(this);
  }

  componentDidMount() {
    this.handleCriteriaSearch();
  }

  handleCriteriaSearch(searchCriteria) {
    FlightService.getInstance().findFlights(searchCriteria)
      .then(flights => {
        this.setState({
          flights: flights
        });
      });
  }

  handleRowClick(flight) {
    const selectedFlights = this.state.selectedFlights;
    const index = selectedFlights.findIndex(selected => selected.id === flight.id);
    if (index >= 0) {
      selectedFlights.splice(index, 1);
    } else {
      selectedFlights.push(flight);
    }
    this.setState({selectedFlights: selectedFlights});
  }

  render() {
    return <div>
      <SearchCriteria handleSubmit={this.handleCriteriaSearch}/>
      <SearchResults flights={this.state.flights}
                     selectedFlights={this.state.selectedFlights}
                     handleRowClick={this.handleRowClick}/>
      <FlightMap className={style.mapContainer} selectedFlights={this.state.selectedFlights}/>
    </div>
  }
}
