import * as React from 'react'
import { EditableMap } from 'shared';

export class FlightManager extends React.Component {

  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div>
        <EditableMap />
      </div>
    )
  }
}