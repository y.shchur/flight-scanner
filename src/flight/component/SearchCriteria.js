import * as React from "react";

export class SearchCriteria extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      searchCriteria: {}
    };

    this.fieldChange = this.fieldChange.bind(this);
    this.submit = this.submit.bind(this);
  }

  fieldChange(event) {
    this.setState({
      searchCriteria: {
        ...this.state.searchCriteria,
        [event.target.name]: event.target.value
      }
    });
  }

  submit(event) {
    event.preventDefault();
    this.props.handleSubmit(this.state.searchCriteria);
  }

  render() {
    return <div>
      <form onSubmit={this.submit}>
        <div className="form-row">
          <div className="col">
            <input name="originCode" type="text" className="form-control" placeholder="From" onChange={this.fieldChange}/>
          </div>
          <div className="col">
            <input name="destinationCode" type="text" className="form-control" placeholder="To" onChange={this.fieldChange}/>
          </div>
          <div className="col">
            <button type="submit" className="btn btn-primary">Search</button>
          </div>
        </div>
      </form>
    </div>
  }
}
