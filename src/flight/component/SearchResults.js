import * as React from "react";
import { Checkbox } from 'shared';

export class SearchResults extends React.Component {


  constructor(props, context) {
    super(props, context);
    this.isSelected = this.isSelected.bind(this);
  }

  isSelected(flightId) {
    return this.props.selectedFlights.findIndex(selected => selected.id === flightId) >= 0;
  }

  render() {
    return <table className="table">
      <thead className="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Origin</th>
        <th scope="col">Departure</th>
        <th scope="col">Destination</th>
        <th scope="col">Arrival</th>
        <th scope="col">Operator</th>
      </tr>
      </thead>
      <tbody>
      {this.createRows()}
      </tbody>
    </table>
  }

  createRows() {
    const flights = this.props.flights;
    const rows = [];
    for (let i = 0; i < flights.length; i++) {
      const flight = flights[i];
      rows.push(
        <tr className={this.isSelected(flight.id) ? 'table-active' : ''}
            key={i}
            onClick={()=>this.props.handleRowClick(flight)}>
          <th scope="row">
            <Checkbox isSelected={this.isSelected(flight.id)} id={flight.id}/>
          </th>
          <td>{flight.originCode}</td>
          <td>{flight.departure}</td>
          <td>{flight.destinationCode}</td>
          <td>{flight.arrival}</td>
          <td>{flight.operator}</td>
        </tr>
      );
    }
    return rows;
  }
}
