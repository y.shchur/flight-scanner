export class AirportTo {
  code;
  name;
  geom;

  constructor(airport) {
    return {...this, ...airport};
  }
}
