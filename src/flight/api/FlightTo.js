export class FlightTo {
  id;
  originCode;
  departure;
  destinationCode;
  arrival;
  operator;

  constructor(flight) {
    return {...this, ...flight};
  }


}
