import {AirportTo} from '../api/AirportTo';

export class AirportService {

  static _instance = null;

  static _airports_url = 'http://localhost:3001/api/airports';

  static getInstance() {
    return this._instance || new AirportService();
  }

  constructor() {
    this.airports$ = this._findAllAirports();
  }

  findAirportsByCodes(codes) {
    return this.airports$.then(airports => airports.filter(airport => codes.includes(airport.code, 0)));
  }

  async _findAllAirports() {
    const url = new URL(AirportService._airports_url);
    return fetch(url)
      .then(resp => resp.json())
      .then(airports => new Map(airports.map(airport => [airport.code, new AirportTo(airport)])));
  }
}
