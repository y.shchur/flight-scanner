import {FlightTo} from "../api/FlightTo";

export class FlightService {

  static _instance = null;

  static _flights_url = 'http://localhost:3001/api/flights';

  static getInstance() {
    return this._instance || new FlightService();
  }


  /**
   * Finds flights by search criteria
   *
   * @param {*} searchCriteria
   * @returns {Promise<FlightTo[]>} promise of flight list
   * @memberof FlightService
   */
  async findFlights(searchCriteria) {
    const url = new URL(FlightService._flights_url);
    if (searchCriteria) {
      Object.keys(searchCriteria).forEach(key => searchCriteria[key] && url.searchParams.append(key, searchCriteria[key]));
    }
    return fetch(url)
      .then(resp => resp.json())
      .then(flights => flights.map(flight => new FlightTo(flight)));
  }
}
