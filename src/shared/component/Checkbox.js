import * as React from 'react';

export class Checkbox extends React.Component {


  constructor(props, context) {
    super(props, context);
    this.onChange = this.onChange.bind(this);
  }

  onChange(event) {
    event.preventDefault();
  }

  render() {
    return <div className="custom-control custom-checkbox my-1 mr-sm-2">
      <input type="checkbox" checked={this.props.isSelected}
             onChange={this.onChange}
             onClick={this.onChange}
             className="custom-control-input"
             id={'checkbox' + this.props.id}/>
      <label className="custom-control-label"
             htmlFor={'checkbox' + this.props.id}
             onClick={this.onChange}></label>
    </div>
  }
}
