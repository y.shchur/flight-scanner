import * as React from "react";
import Aviator from "aviator";

export class Header extends React.Component {

  constructor(props, context) {
    super(props, context);

    this.navigate = this.navigate.bind(this);
  }

  navigate(event, url) {
    event.preventDefault();
    Aviator.navigate(url);
  }

  render() {
    return <nav className="navbar navbar-light bg-light navbar-collapse navbar-expand-lg shadow-sm">
      <a className="navbar-brand" href={Aviator.hrefFor('/home/')} onClick={(event) => this.navigate(event, '/home/')}>Flight Scanner</a>
      <ul className="navbar-nav mr-auto">
        <li className="nav-item">
          <a className="nav-link" href={Aviator.hrefFor('/flights/')} onClick={(event) => this.navigate(event, '/flights/')}>Flights</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href={Aviator.hrefFor('/manage/')} onClick={(event) => this.navigate(event, '/manage/')}>Manage</a>
        </li>
      </ul>
    </nav>
  }
}
