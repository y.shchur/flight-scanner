import React from 'react';
import {Feature} from 'ol'
import {Point} from 'ol/geom'
import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import {LayerExplorer} from './LayerExplorer';
import {MapView} from './MapView';

export class EditableMap extends React.Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      layers: []
    };

    this.handleLayerSelection = this.handleLayerSelection.bind(this);
  }

  componentDidMount() {
    const layer = new VectorLayer({
      source: new VectorSource({
        features: [new Feature(new Point([16.880372, 51.104411]).transform('EPSG:4326', 'EPSG:3857'))]
      })
    });
    layer.setProperties({'name': 'Test layer'})
    this.setState({
      layers: [
        layer
      ]
    })
  }

  handleLayerSelection(layerName) {

  }


  render() {
    return <div className="row">
      <LayerExplorer layers={this.state.layers}
                     onLayerSelection={this.handleLayerSelection}/>
      <MapView layers={this.state.layers}/>
    </div>
  }
}

export default EditableMap;
