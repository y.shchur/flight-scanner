import React from 'react'
import {Checkbox} from 'shared';

export class LayerExplorer extends React.Component {

  constructor(props, context) {
    super(props, context);

  }

  isSelected(layerName) {
    return false;
  }

  render() {
    return (
      <div className="col-2">
        <div>Layers</div>
        <ul className="list-group list-group-flush">
          {this._createLayers()}
        </ul>
      </div>
    );
  }

  _createLayers() {
    const layers = [];
    for (let layer of this.props.layers) {
      let layerName = layer.getProperties().name;
      const listElement =
        <li key={layerName} className="list-group-item"
            onClick={() => this.props.onLayerSelection(layerName)}>
          <Checkbox isSelected={this.isSelected(layerName)}/>{layerName}
        </li>
      layers.push(listElement);
    }
    return layers;
  }
}
