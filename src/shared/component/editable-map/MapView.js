import React from 'react';
import { Map, View } from 'ol'
import TileLayer from 'ol/layer/Tile'
import { OSM } from 'ol/source'
import * as proj from 'ol/proj'

export class MapView extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            layers: []
        }
    }

    componentDidUpdate() {
        for (let layer of this.props.layers) {
            this.map.addLayer(layer);
        }
    }

    componentDidMount() {
        this.map = new Map({
            target: this.refs.mapContainer,
            layers: [
                new TileLayer({
                    source: new OSM()
                }), ...this.props.layers
            ],
            view: new View({
                center: proj.transform([16, 51], 'EPSG:4326', 'EPSG:3857'),
                zoom: 5
            })
        });
    }
    render() {
        return (
            <div className="col-10" ref="mapContainer">Map</div>
        )
    }
}