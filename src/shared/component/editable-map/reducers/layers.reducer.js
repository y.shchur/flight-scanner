const layers = (state = [], action) => {
  switch (action.type) {
    case 'ADD_LAYER':
      return 'add layer';
    case 'REMOVE_LAYER':
      return 'remove layer';
  }
};

export default layers;
