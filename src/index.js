import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import * as serviceWorker from './serviceWorker';
import {App} from "./App";
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import layers from './shared/component/editable-map/reducers/layers.reducer';

const store = createStore(layers);

ReactDOM.render(
  <Provider>
    <App/>
  </Provider>,
  document.getElementById('root'));
document.getElementById('root').setAttribute('class', 'container-fluid');

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
