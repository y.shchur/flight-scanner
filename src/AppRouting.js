import * as React from "react";
import Aviator from "aviator";
import { FlightOverview, FlightManager } from 'flight';

export class AppRouting extends React.Component {

  currentComponent = null;

  constructor(props, context) {
    super(props, context);
    this.state = {currentComponent: []};
  }


  componentDidMount() {
    this._initRoutes();
  }

  setCurrentComponent(component) {
    this.setState({currentComponent: component});
    console.log(this.state);
  }


  render() {
    return this.state.currentComponent;
  }

  _initRoutes() {
    Aviator.setRoutes({
        '/': {
          target: this,
          '/': () => Aviator.navigate('/home')
        },
        '/home': {
          target: this,
          '/': () => this.setState({currentComponent: null})
        },
        '/flights': {
          target: this,
          '/': () => this.setState({currentComponent: <FlightOverview/>})
        },
        '/manage': {
          target: this,
          '/': () => this.setState({currentComponent: <FlightManager/>})
        }
      }
    );
    Aviator.dispatch();
  }
}
