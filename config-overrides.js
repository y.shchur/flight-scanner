const path = require('path');

module.exports = function override(config, env) {
    //do stuff with the webpack config...
    config.resolve = {
        modules: [
            path.resolve(__dirname + '/src'),
            path.resolve(__dirname + '/node_modules')
        ]
    }
    return config;
}